# README #

ZIVHACK 2014 - RuleEngine in Python by Abhishek & Pradeep [RuleHackers]

A Rule Engine from scratch with a User Interface for entering the rules :)

### What is this repository for? ###

* Quick summary
To get started with Building the Rule Engine from Scratch. Our rule engine evalutes the rule using INFIX expression evalution. 

### How do I get set up? ###

* Summary of set up
Setup python & flask (module).

* Configuration
No additional configuration required.

* Dependencies
Require Python 2.7, Flask (python module)

* Deployment instructions
Run the "python webserver.py" and it will start the Web server on default port "5000".
Sample data is available in files (data_input.json & rule_data.txt)

### Who do I talk to? ###

* Abhishek Nair (abhishek.alchemist@gmail.com)
* Pradeep Bishnoi (pradeepbishnoi@gmail.com)
