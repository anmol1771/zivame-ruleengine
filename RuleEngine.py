from Stack import Stack
from Operation import Operation
import copy
from JSONParser import JSONParser as parse
from RuleParser import RuleParser

class RuleEngine():
    ''' core RuleEngine to process rules with associated data '''
    # sample member variable for test
    __rule_sample = '[{"category_id"}in{("200","201","202","203","204")}]'
    
    # all private member variables 
    __conditionBracketOpen = '['
    __conditionBracketClose = ']'
    __stringBracketOpen = '{'
    __stringBracketClose = '}'
    __greaterThan = '>'
    __greaterThanEqualTo = '>='
    __lessThan = '<'
    __lessThanEqualTo = '<='
    __equalTo = '='
    __notEqualTo = '!='
    __or = 'or'
    __and = 'and'
    __in = 'in'
    __notIn = 'notin'
    __operators = [__equalTo,__greaterThan,__lessThan, __in[0], __notIn[0]]
    __operatorsList = [__equalTo,__greaterThan,__lessThan, __in, __greaterThanEqualTo, __lessThanEqualTo, __and, __or, __notIn, __notEqualTo]
    __cartTotalKey = 'cart_total'
    __dataKeys = [__cartTotalKey,'price','category_id']

    def __init__(self):
        pass

    def run(self,jsonData,cartTotal,ruleMap):
        ''' run each rule for all data sets '''
        rulePassMap = {}
        for rule in ruleMap:
            opStack = self.makeStack(rule['rule'])
            
            # run this rule for whole data set
            for data in jsonData:
                opStackTemp = copy.deepcopy(opStack)
                currentProductId = data['product']['id']
                if rulePassMap.has_key(currentProductId) == False:
                    rulePassMap[currentProductId] = [] # make list of rules passed
                
                if self.processRule(opStackTemp,data['product'],cartTotal) == True:
                    # TODO assuming ruleId will be integer
                    print "Action is ", rule['action']
                    rulePassMap[currentProductId].append(rule['action'])

        return rulePassMap

    def makeStack(self,rule):
        ''' make stack of the rule string to make processing easier '''
        tempString = ''
        tempOperator = ''
        opStack = Stack()
        ruleLength = len(rule)
        i = 0 # counter
        while i != ruleLength:
            if(rule[i] == self.__conditionBracketOpen):
                opStack.push(rule[i])
            # string handling
            elif(rule[i] == self.__stringBracketOpen):
                tempString = ''
                i += 1
                while(rule[i] != self.__stringBracketClose):
                    tempString += rule[i]
                    i += 1
                tempString = tempString.replace('"','')
                print "pushing tempString: " , tempString
                opStack.push(tempString)
            elif(rule[i] in self.__operators):
                if(rule[i+1] == self.__equalTo ):
                    tempOperator = rule[i:i+len(self.__equalTo)]
                    opStack.push(tempOperator)
                    i += 1
                elif(rule[i+1] == self.__in[1]):
                    tempOperator = rule[i:i+len(self.__in)]
                    opStack.push(tempOperator)
                    i += 1
                elif(rule[i+1] == self.__notIn[1]):
                    tempOperator = rule[i:i+len(self.__notIn)]
                    opStack.push(tempOperator)
                    i += len(self.__notIn) - 1
                else:
                    opStack.push(rule[i])
            elif(rule[i] == self.__conditionBracketClose):
                opStack.push(rule[i])
            elif(rule[i] == self.__and[0]):
                tempOperator = rule[i:i+len(self.__and)]
                opStack.push(tempOperator)
                i += len(self.__and) - 1 
            elif(rule[i] == self.__or[0]):    
                tempOperator = rule[i:i+len(self.__or)]
                opStack.push(tempOperator)
                i += len(self.__or) - 1
            i += 1
        return opStack            

    def hasPrecedence(self,left,right):
        if(right == self.__conditionBracketOpen or right == self.__conditionBracketClose ):
            return False
        else:
            return True

    def processRule(self,opStack,entityMap,cartTotal):
        ''' process the tokenized rule query over given data '''
        print "RUNNING FOR STACK: ", opStack.getStack()
        opStack.reverseStack()
        
        operation = Operation()
        valueStack = Stack()
        operatorStack = Stack()
   
        while opStack.isEmpty() != True:
            print "BEGIN: ",valueStack.getStack(),operatorStack.getStack()
            token = opStack.pop()
            if token == self.__conditionBracketOpen:
                print token
                operatorStack.push(token)
            elif token == self.__conditionBracketClose:
                while operatorStack.peek() != self.__conditionBracketOpen:
                    operator = operatorStack.pop()
                    value = valueStack.pop()
                    entity = valueStack.pop()
                    print "VALUES: ",entity,operator,value
                    result = operation.operate(entity,operator,value)
                    print "RESULT: ",result
                    valueStack.push(result)
                operatorStack.pop()
                print "CHECK: ",valueStack.getStack(),operatorStack.getStack()
            elif token in self.__operatorsList:
                while (operatorStack.isEmpty() != True) and self.hasPrecedence(token,operatorStack.peek()):
                    operator = operatorStack.pop()
                    value = valueStack.pop()
                    entity = valueStack.pop()
                    print "VALUES: ",entity,operator,value
                    result = operation.operate(entity,operator,value)
                    print "RESULT: ",result
                    valueStack.push(result)
                print token
                operatorStack.push(token)
            else:
                if token in self.__dataKeys:
                    entityKey = token
                    if entityKey == self.__cartTotalKey:
                       entity = cartTotal
                    else:
                        entity = entityMap[entityKey]
                    print "entity: ", entity
                    valueStack.push(entity)
                else:
                    valueStack.push(token)
                    print "value: ", token
                print valueStack.getStack(),operatorStack.getStack()

        
        print opStack.getStack()
        print operatorStack.getStack()
        print valueStack.getStack()
        return valueStack.pop()

if __name__ == '__main__':
    ''' test code '''
    jsonDataParser = parse()
    myParsedObj = jsonDataParser.parseData('data_input.json')
    cartTotal = jsonDataParser.cart_total

    ruleMapList = RuleParser().parse()
    # print ruleMapList
    r = RuleEngine()
    rulePassMap = r.run(myParsedObj, cartTotal, ruleMapList)
    print rulePassMap


    # entityMap = {"price" : "400", "category_id" : "[200,201]" }
    # entityMap = {u'product': {u'price': u'1000', u'category_id': [u'200', u'201', u'202'], u'id': u'1'}}
    # jsonData = [{u'product': {u'price': u'1000', u'category_id': [u'200', u'201', u'202'], u'id': u'1'}}, {u'product': {u'price': u'2000', u'category_id': [u'200'], u'id': u'2'}}, {u'product': {u'price': u'5000.33', u'category_id': [u'201', u'202'], u'id': u'3'}}, {u'product': {u'price': u'2500.50', u'category_id': [u'203'], u'id': u'4'}}]
    # r.processRule(l,entityMap['product'], cartTotal)
    # ruleMapList = [{"rule" : '[{"category_id"}in{("200","201","202","203","204")}]', "action" : "" , "ruleId" : 1}]
    