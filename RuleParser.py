import json
import re

class RuleParser(object):
    ''' Parse the rule'''
    def __init__(self):
        pass
    
    def parse(self, filename="rule_data.txt"):
        ''' parse json string '''
        data_file = open(filename,"r")
        rule_bool = False
        data_map_list = []
        file_as_string = ""
        rule_id_counter = 1
        for line in data_file.readlines():
            print "Parsing rule"
            if line.lower().strip()=="rule_start":
                rule_bool=True
            elif line.lower().strip()=="rule_end":
                temp_map = {}
                temp_list = file_as_string.lower().split('then')
                temp_map['rule'] = temp_list[0]
                temp_map['action'] = temp_list[1]
                temp_map['rule_id'] = rule_id_counter
                data_map_list.append(temp_map)
                file_as_string="" # Clear the file string data
                rule_id_counter += 1 # Increment the rule id counter
                rule_bool=False
            elif rule_bool:
                file_as_string = file_as_string + ''.join(line).replace("\n","").replace("\t","")
                file_as_string = re.sub(r'\s+', '',file_as_string)
        return data_map_list        

if __name__=="__main__":
    ''' test code '''
    print "Rule Parser"
    print len(RuleParser().parse('rule_data.txt'))
    print RuleParser().parse('rule_data.txt')
    