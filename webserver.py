from flask import Flask, jsonify, request, render_template
from RuleEngine import RuleEngine
from JSONParser import JSONParser as parse
from RuleParser import RuleParser
import json

app = Flask(__name__, static_url_path='/static')

@app.route('/', methods = ['POST', 'GET'])
def home_page():
    '''
    Show the user interface to gather the Rule, JSON & show the processed Rule data
    '''
    if request.method == 'POST':
        #ToDo : Handle the pass
        return "POST Method called"
    else:
        return render_template('hello.html', name="Rule Engine")
    
@app.route('/ruleengine', methods = ['POST'])
def rule_engine():
    '''
    To process the Rule data for a POST request in background
    '''
    if request.method == 'POST':
        jsonDataParser = parse()
        jsonDataInput = open("temp.json","w")
        jsonDataInput.write(request.form['json_data']) 
        jsonDataInput.close()       
        myParsedObj = jsonDataParser.parseData("temp.json")#'data_input.json')
        cartTotal = jsonDataParser.cart_total
        print "cart tttotal", cartTotal
        ruleDataInput = open("temp.txt","w")
        ruleDataInput.write(request.form['rule_data']) 
        ruleDataInput.close()       
        ruleMapList = RuleParser().parse("temp.txt")
        print "ruleMaplist",ruleMapList
        print "JSON dump", myParsedObj
        r = RuleEngine()
        rulePassMap = r.run(myParsedObj, cartTotal, ruleMapList)
        return json.dumps(rulePassMap)
    

@app.route('/rest/<int:id>', methods = ['GET'])
def dummy(id):
    '''
    Dummy REST API method to test our Webserver
    '''
    return jsonify( { 'task': id } )
    
if __name__ == '__main__':
    '''Run the WebApp server using Flask'''
    app.run(host='0.0.0.0', port=5000, debug = True)
